import axios from "axios";

class Api {
  static async getUserInfo(username) {
    try {
      const res = await axios.get("https://api.github.com/users/" + username);
    } catch (err) {
      console.warn("Erro na api");
    }
  }
}

export default Api