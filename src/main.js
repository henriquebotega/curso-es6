import api from "./api";

class App {
  constructor() {
    this.repositories = [];

    this.formEl = document.getElementById("repo-form");
    this.listEl = document.getElementById("repo-list");
    this.inputEl = document.querySelector("input[name=repository]");

    this.registerHandlers();
  }

  registerHandlers() {
    this.formEl.onsubmit = event => this.addRepository(event);
  }

  setLoading(loading = true) {
    if (loading) {
      let loadingEl = document.createElement("span");
      loadingEl.appendChild(document.createTextNode("Aguarde..."));
      loadingEl.setAttribute("id", "loading");

      this.formEl.appendChild(loadingEl);
    } else {
      document.getElementById("loading").remove();
    }
  }

  async addRepository(event) {
    event.preventDefault();

    const repoInput = this.inputEl.value;

    if (repoInput.length == 0) {
      return;
    }

    this.setLoading();

    try {
      const res = await api.get("/repos/" + repoInput);

      const {
        name,
        description,
        html_url,
        owner: { avatar_url }
      } = res.data;

      this.repositories.push({
        name,
        description,
        avatar_url,
        html_url
      });

      this.inputEl.value = "";
      this.render();
    } catch (error) {
      console.log("O repositorio nao existe");
    }

    this.setLoading(false);
  }

  render() {
    this.listEl.innerHTML = "";

    this.repositories.forEach(itemAtual => {
      let imgEl = document.createElement("img");
      imgEl.setAttribute("src", itemAtual.avatar_url);

      let bEl = document.createElement("b");
      bEl.appendChild(document.createTextNode(itemAtual.name));

      let descriptionEl = document.createElement("p");
      descriptionEl.appendChild(document.createTextNode(itemAtual.description));

      let linkEl = document.createElement("a");
      linkEl.setAttribute("target", "_blank");
      linkEl.appendChild(document.createTextNode("Acessar"));
      linkEl.setAttribute("href", itemAtual.html_url);

      let liEl = document.createElement("li");
      liEl.appendChild(imgEl);
      liEl.appendChild(bEl);
      liEl.appendChild(descriptionEl);
      liEl.appendChild(linkEl);

      this.listEl.appendChild(liEl);
    });
  }
}

new App();
