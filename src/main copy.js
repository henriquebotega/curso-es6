import { somador as somaFunction, subtrador } from "./funcoes";
import Api from './api'

class List {
  constructor() {
    this.data = [];
  }

  add(data) {
    this.data.push(data);
  }
}

class TodoList extends List {
  constructor() {
    super();

    this.todo = [];
    this.usuario = "Diego";
  }

  addTodo() {
    this.todo.push("Novo todo");
  }

  mostraUsuario() {
    console.log(this.usuario);
  }
}

const minhaLista = new TodoList();

document.getElementById("btnNovo").onclick = function() {
  minhaLista.add("NovoTodo");
  minhaLista.mostraUsuario();
  minhaLista.addTodo();
};

class Matermatica {
  static soma(a, b) {
    return a + b;
  }
}

var res = Matermatica.soma(1, 3);
console.log(res);

const arr = [1, 3, 4, 5, 8, 9];

const newArr = arr.map((obj, i) => {
  return obj + i * 2;
});
console.log(newArr);

const suma = arr.reduce((total, n) => {
  return total + n;
}, 0);
console.log(suma);

const fil = arr.filter(obj => {
  return obj % 2 == 0;
});
console.log(fil);

const fin = arr.find(obj => {
  return obj == 4;
});
console.log(fin);

const usuario = {
  nome: "anderson",
  idade: 35,
  empresa: {
    titulo: "sabium",
    local: "maringa"
  }
};

const { nome, ...demais } = usuario;
console.log(nome);
console.log(demais);

const usuario2 = { ...usuario, nome: "Gabriel" };
console.log(usuario2);

const novoArr = [1, 2, 3, 4, 5];
const [a, b, ...c] = novoArr;
console.log(a);
console.log(b);
console.log(c);

function somaTotal(a, b, ...params) {
  return a + b + params.reduce((t, n) => t + n);
}

var st = somaTotal(1, 3, 7, 9, 6);
console.log(st);

const a1 = [1, 2, 3];
const a2 = [4, 5, 6];
const a3 = [...a1, ...a2];
console.log(a3);

console.log(somaFunction(6, 2));
// console.log(somador(5, 5));
console.log(subtrador(10, 3));

const minhaPromise = () => new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('ok')
  }, 2000)
})

minhaPromise().then((obj) => {
  console.log('obj', obj)
}).catch((error) => {
  console.log('error', error)
})

const minhaFuncao = async() => {
  const res = await minhaPromise()
  return res
}

minhaFuncao().then(res => {
  console.log('res', res)
})

Api.getUserInfo("diego3g");
Api.getUserInfo("diego123456789");
